# TikBot API

**This API is intended to be used in a network-private context, thus it provides no authorization/rate limiting.**

## Motivation

TikTok regularly change their security/spam prevention setup.
I got tired of maintaining it, so I looked for an alternative solution.
The best solution was the fantastic [tiktok-scraper](https://github.com/drawrowfly/tiktok-scraper) library, which is written in Node.JS.

This simple API serves as the backend for the [tikbot](https://gitlab.com/tikbot) Telegram bot.


## Installation

- Install required packages via `yarn`
- Start the server using `node app.js`

## Accepted configuration options

### Proxy

You may set a proxy with the `TIKTOK_PROXY` variable.

Supported proxies:

- SOCKS5 (`socks5://127.0.0.1:8080`)
- HTTP (`127.0.0.1:8080`)

These limitations are described in the [`tiktok-scraper` docs](https://github.com/drawrowfly/tiktok-scraper#options).

### Sentry

Please set the Sentry DSN with the `SENTRY_DSN` environment variable.

## API 

The app exposes one route, `POST /api/v1/video/`. Include the `video` parameter in the request body.

This endpoint returns all video meta found by the [tiktok-scraper](https://github.com/drawrowfly/tiktok-scraper) library.
