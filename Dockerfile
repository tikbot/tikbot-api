FROM node
WORKDIR /app
ADD . /app
RUN yarn install
EXPOSE 3000
ENTRYPOINT ["node", "app.js"]