const express = require("express");
const TikTokScraper = require("tiktok-scraper");
const bodyParser = require("body-parser");
const validUrl = require('valid-url');
const asyncRedis = require("async-redis");

const Sentry = require('@sentry/node');

const client = asyncRedis.createClient(process.env.REDIS_URL);

const app = express();
if (process.env.SENTRY_DSN) {
    Sentry.init({
        dsn: process.env.SENTRY_DSN,
        captureUnhandledRejections: true
    });
} else {
    console.warn("Sentry disabled, please set the SENTRY_DSN environment variable.");
}

if (!process.env.TIKTOK_PROXY) {
    console.warn("Proxy disabled, please set the TIKTOK_PROXY environemnt variable.");
}

app.use(Sentry.Handlers.requestHandler());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.post("/api/v1/video/", async (req, res) => {
    const {video:videoUrl} = req.body;

    if (!validUrl.isHttpsUri(videoUrl))
        return res.status(400).json({message: "Invalid video property given"});

    const cachedData = await client.get(encodeURI(videoUrl));

    Sentry.configureScope(scope => scope.setTransactionName("Get TikTok Video"));

    if (!cachedData) {
        Sentry.addBreadcrumb({category: "cache", message: `Could not find video ${videoUrl} in cache`})
        let videoMeta;

        try {
            videoMeta = await TikTokScraper.getVideoMeta(videoUrl, {
                proxy: process.env.TIKTOK_PROXY || ''
            })
        } catch (e) {
            Sentry.captureException(e, {tags: {"video": videoUrl}});
            return res.status(500).json({
                "message": e.toString()
            });
        }

        console.info(`Fetched video ${videoUrl}`);
        client.set(encodeURI(videoUrl), JSON.stringify(videoMeta), 'EX', 60 * 60)
        return res.json(videoMeta);
    } else {
        console.info(`Answering with cached content for ${videoUrl}`);
        return res.json(JSON.parse(cachedData));
    }
});

app.use(Sentry.Handlers.errorHandler({
    shouldHandleError(error) {
        return error.status === 500;
    }
}));


const appPort = 3000;
app.listen(appPort, () => console.info(`tikbot-api listening on ${appPort}`));
